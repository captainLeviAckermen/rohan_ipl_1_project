
module.exports = {numberOfMatchesPlayedPerYear , numberOfMatchesWonPerTeamPerYear , extraRunsPerTeamIn2016 , topEconomicalBowlers2015 };


// First Function for Number of matches played per year for all the years in IPL.
function numberOfMatchesPlayedPerYear(inputData){
    let matchesPerYear = {};
    for(let index=0;index<inputData.length;index++){
       let row = inputData[index];
       if(matchesPerYear[row.season] === undefined){
           matchesPerYear[row.season] = 1;
       } else{
           matchesPerYear[row.season] += 1;
       }
    }
    return matchesPerYear;
}


// Second Function for Number of matches won per team per year in IPL.
function numberOfMatchesWonPerTeamPerYear(inputData){
    let matchesWonPerTeamPerYear = {};
    for(let index=0;index<inputData.length;index++){
        let row = inputData[index];
        if(row.result !== 'no result'){
        if(row.season in matchesWonPerTeamPerYear){
            if(matchesWonPerTeamPerYear[row.season][row.winner]){
                matchesWonPerTeamPerYear[row.season][row.winner]++;
            } else {
                matchesWonPerTeamPerYear[row.season][row.winner] = 1;
        }
    } else {
        matchesWonPerTeamPerYear[row.season] = {};
        matchesWonPerTeamPerYear[row.season][row.winner] = 1;
    }
    }
 } return matchesWonPerTeamPerYear;
}


//Third function for Extra runs conceded per team in the year 2016
function extraRunsPerTeamIn2016(dataMatches,dataDeliveries){
    let extraRuns2016 = {};
    let initialId2016 = 0;
    let lastId2016 = 0;
    for(let index=0;index<dataMatches.length;index++){ 
        if(dataMatches[index].season === '2016'){
            if(!initialId2016){
                initialId2016 = parseInt(dataMatches[index].id , 10);  
            }
            lastId2016 = parseInt(dataMatches[index].id , 10);
        }
     }
     
     for(let index=0;index<dataDeliveries.length;index++){
        let row = dataDeliveries[index];
        if(parseInt(row['match_id'] , 10) >= initialId2016 && parseInt(row['match_id'] , 10) <= lastId2016 ){ 
           if(extraRuns2016[row.bowling_team]){
            extraRuns2016[row.bowling_team] = parseInt(extraRuns2016[row.bowling_team] , 10)+parseInt(row.extra_runs , 10);  
           }
           else{
            extraRuns2016[row.bowling_team] = parseInt(row.extra_runs , 10); 
           }
        }
     }
    return extraRuns2016;
 }

 //Fourth function for top 10 economical bowlers in the year 2015
 function topEconomicalBowlers2015(dataMatches,dataDeliveries){
    let object = {};
    let top10EconomicalBowler = {};
    for(let index=0;index<dataMatches.length;index++){
     if(dataMatches[index].season === '2015'){
       for(let rowIndex=0;rowIndex<dataDeliveries.length;rowIndex++){
         if(dataMatches[index].id === dataDeliveries[rowIndex].match_id){
            if(object[dataDeliveries[rowIndex].bowler]){
                object[dataDeliveries[rowIndex].bowler][0] += parseInt(dataDeliveries[rowIndex].total_runs , 10);
                object[dataDeliveries[rowIndex].bowler][1] ++;
               } else{
                    object[dataDeliveries[rowIndex].bowler] = [];
                    object[dataDeliveries[rowIndex].bowler].push(parseInt(dataDeliveries[rowIndex].total_runs , 10),1);
                    }
                }
            }
        }
    }
    let array = [];
    for(let val in object){
       let data = (object[val][0])*6/object[val][1];
       array.push([val,data]);
     }
        array.sort(function (a,b){
        return a[1]-b[1]; });
    
       for(let index=0;index<=10;index++){
        top10EconomicalBowler[array[index][0]] = array[index][1];
        } return top10EconomicalBowler;
}