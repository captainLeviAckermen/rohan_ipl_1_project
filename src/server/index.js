const fs = require("fs");
const csv= require('csvtojson');
const iplFunctions = require("./ipl.js");

const matchesPath = "../data/matches.csv";
const deliveriesPath = "../data/deliveries.csv";


// Read file 1
csv()
.fromFile(matchesPath)
.then((matchesArray) =>{
    
    // Read file 2
    csv()
    .fromFile(deliveriesPath)
    .then((deliveriesArray) => {
        
         console.log(iplFunctions.numberOfMatchesPlayedPerYear(matchesArray));
           fs.writeFile("../output/matchesPerYear.json", JSON.stringify(iplFunctions.numberOfMatchesPlayedPerYear(matchesArray)), function (err) {
        if (err) {
            console.error("An error occured while writing JSON Object to File.");
        }
        else
        console.log("Number of matches played per year JSON file has been saved.");
    }); 
        console.log(iplFunctions.numberOfMatchesWonPerTeamPerYear(matchesArray,deliveriesArray));
            fs.writeFile("../output/numberOfMatchesWonPerTeamPerYear.json", JSON.stringify(iplFunctions.numberOfMatchesWonPerTeamPerYear(matchesArray)), function (err) {
        if (err) {
              console.error("An error occured while writing JSON Object to File.");
        }
        else
        console.log("Number of matches won per year JSON file has been saved.");
    }); 
        console.log(iplFunctions.extraRunsPerTeamIn2016(matchesArray,deliveriesArray));
            fs.writeFile("../output/extraRunsPerTeamIn2016.json", JSON.stringify(iplFunctions.extraRunsPerTeamIn2016(matchesArray,deliveriesArray)), function (err) {
        if (err) {
            console.error("An error occured while writing JSON Object to File.");
        }
        else
        console.log(" Extra runs conceded per team in 2016 season JSON file has been saved.");
    });
        console.log(iplFunctions.topEconomicalBowlers2015(matchesArray,deliveriesArray));
           fs.writeFile("../output/topEconomicalBowlers2015.json", JSON.stringify(iplFunctions.topEconomicalBowlers2015(matchesArray,deliveriesArray)), function (err) {
        if (err) {
            console.error("An error occured while writing JSON Object to File.");
        }
        else
        console.log("top 10 economical bowlers in 2015 JSON file has been saved.");
    });

   
    });

});



fs.readFile("../data/matches.csv", "utf-8", function(err,data){
    if(err){
        console.error(err);
        console.error("An error occurred while reading the matches.csv file");
    } else{
        console.log(typeof data);
    }
});
fs.readFile("../data/deliveries.csv", "utf-8", function(err,data){
    if(err){
        console.error(err);
        console.error("An error occurred while reading the deliveries.csv file");
    } else{
        console.log(typeof data);
    }
});

